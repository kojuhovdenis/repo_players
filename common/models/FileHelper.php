<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
/**
 * Helper со вспомогательными функциями работы с файлами
 */
class FileHelper extends File
{
    /**
     * Сохранение файла (примеры параметров)
     * 
     * @param UploadedFile $file - файл
     * @param string $path - директория для сохранения файла
     * @param string $filePrefix - префикс имени файла для сохранения
     * @param boolean $random
     * @return string имя файла или null
     */
    public static function saveFile ($file, $path, $filePrefix = null, $random = true){
        
        // напиши тут реализацию
        $sdfsdf = 'dsfsdfdsf';
       
    }


    /**
     * Возвращает расширение файла
     *
     * @return string
     */
    public function getExtension($file) {

        $result = '';
        if ($file) {
            $result = substr(strrchr($file, '.'), 1);
        }

        return $result;
    }

    private static function generateRandomMD5(){
        return md5((double)microtime()*1000000);
    }

    public static function MarkAsDeleted($id){
        $model = self::findOne($id);
        if ($model){
            $model->deleted_at = time();
            $model->deleted_by = \Yii::$app->user->id;
            $model->save();
        }
    }
}
