<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
//use app\models\GameEvent;
//use app\components\FilePath;
/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $path
 * @property integer $type_id
 * @property integer $player_id
 * @property integer $team_id
 * @property string $comment
 * @property string $name
 * @property string $date_create
 * @property string $date_start
 * @property string $date_end
 * @property integer $training_id
 *
 * @property FileType $type
 * @property Player $player
 */
class File extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'player_id', 'team_id','exercise_id', 'training_id',
                'num', 'game_event_id', 'user_create', 'branch_id', 
                'report_id', 'user_update'], 'integer'],
            [['date_create', 'game_event_id', 
               'user_create', 'branch_id', 
               'date_start', 'date_end', 'game_id', 'date_update', 
               'deleted_at', 'deleted_by', 'date'], 'safe'],
            [['name'], 'string', 'max' => 125],
            [['comment'], 'string', 'max' => 145],
            [['filename'], 'required', 'on' => 'update']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'name',
            'type_id' => 'Type ID',
            'player_id' => 'Player ID',
            'team_id' => 'Группа',
            'comment' => 'Описание',
            'date_create' => 'Date Create',

            'filename' => 'Название файла',
            'date_start' => 'Начало периода',
            'date_end' => 'Конец периода',
            'tournament_id' => 'Турнир'
        ];
    }

    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name', 'team_id', 'date_start', 'date_end'];
        return $scenarios;
    }

    public function beforeSave($insert)
    {
// вернешь как у тебя будут userы
//        if($this->isNewRecord)
//        {
//            $this->date_create = date('Y-m-d H:i:s');
//            $this->user_create = Yii::$app->user->id;
//        }
//        else {
//            $this->date_update = date('Y-m-d H:i:s');
//            $this->user_update = Yii::$app->user->id;
//        }
//        return parent::beforeSave($insert);
    }

    /**
     * Возвращает название файла без расширения
     *
     * @return string
     */
    public function getFileName() {
        $result = '';
        if ($this->name) {
            $p = strrpos($this->name, '.');
            $result = ($p > 0) ? substr($this->name, 0, $p) : $this->name;
        }
        return $result;
    }
}
