<?php

namespace frontend\controllers;
use app\models\Player;
use common\models\File;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Request;

class PlayerController extends AppController
{

    public function actionIndex(){

        if (Yii::$app->request->isAjax){
            //$this->debug($_GET);
            $this->debug(Yii::$app->request->get());
            return 'test from actionIndex';
        }


        $players = Player::find()
            ->orderBy('id')
            ->asArray()
            ->all();

//        $players = [
//            ['id'=>'1', 'name' => 'Denis'],
//            ['id'=>'2', 'name' => 'Mark']
//        ];

        //$this->debug($hi);

        return $this->render('showTablePlayers', compact('players'));
    }


    public function actionView($id){

        $this->layout = "blank"; //Шаблон, заменен на пустой

        /*  -сгенерируй crud по моей таблице, посмотри как работает действие view
         - замени их вьюху на свою view
        */

        if ($id == null) {
            return "Не задан id!";
        }

        //$model = null;// Player:: ищем модель игрока
        $model = Player::find()
            ->where(['id' => $id])
            ->one();

        //File::ищем файлы по игроку;
        if ($model == null) {
            $video = [];
        }else{
            $video = File::find()
                ->where(['player_id' => $id])
                ->all();
        }

        // формируем провайдер данных для виджета GridView внутри вьюхи _tab_video
        $dataProvider = new ArrayDataProvider([
            'allModels' => $video,
            'pagination' => false
        ]);

        //$data = []; // массив переменных, которые необходимы во вьюхе
        $data = ['model' => $model,
                'video' => $dataProvider];

        return $this->render('view', compact('data'));
    }
}