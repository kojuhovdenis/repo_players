<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;
use app\models\GameSearch;
use app\models\Game;
use app\modules\video\models\File;
use yii\helpers\Html;

class FileController extends Controller
{
    public $layout = '@app/modules/video/views/layouts/main';
    public function behaviors()
    {
        return [
           'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                     // видеооператор Академии, админ
                      [
                         'actions' => ['admin', 'view', 'add-video', 'ajax-reload-game-list'],               
                         'allow' => true,
                         'roles' => ['videographerAcademy', 'admin']
                    ],
                ]
            ]
        ];
    }   
    /**
     * Список всех игр, доступных видеооператору для заливки видео голов
     * не заливают в основном
     * 
     * @return mixed
     */
    public function  actionAdmin () {
        $searchModel = new GameSearch();
        $searchModel->academy = 1;
        //  выведем только доступные оператору
        $searchModel->available_videogroup = 1; 
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams);

        return $this->render('game',['searchModel' => $searchModel,
                                      'dataProvider' => $dataProvider]);    
        
    }
    public function  actionView ($id) {
        $model = $this->findModel($id);
        $players = new \yii\db\Query();
        $players->select('game_attendance.*')->from('game_attendance')
                ->leftJoin('data.player','data.player.id = player_id' )
                ->where('coach_id is null and game_id='.$model->id)
                ->orderBy('amplua_id asc');
        $playerlist = $players->all();

         return $this->render('view-game', [
                'model' => $model,
                'players' => $playerlist
            ]);
    }
    /**
     * Добавление видео гола
     * 
     * @return type
     */
    public function actionAddVideo(){
         $video = new File(["type_id"=>5]);
         $game_id = Yii::$app->request->post('game_id');
         if (!Yii::$app->request->isAjax) {
             if (!empty($game_id)) {
                 $video->attributes = Yii::$app->request->post('File');
                 // сохраним видео
                File::saveVideo($video);
                return $this->redirect(['view', 'id' => $game_id]);
             }
             else {
                 \Yii::$app->getSession()->setFlash('error', 'Не задана игра');
                 return $this->redirect(['video/game']);
             }
         }
         else {
             // отрендерим форму добавления видео к голу
             echo $this->renderPartial('academy/_add_video', ['file' => $video, 
                 'events' =>\app\models\GameEvent::getEventsList($game_id), 'game_id' => $game_id]);
         }
    }
    /**
     * По параметрам перезагружает список игр в добавлении видео игры
     */
    public function actionAjaxReloadGameList(){
        $month = Yii::$app->request->post('month');
        $team_id = Yii::$app->request->post('team');
        $day = Yii::$app->request->post('day');
        // выберем список игр по параметрам
        $gameList = [];
        $gameList = Game::getGameListByMonth($month, $team_id, $day);
        echo Html::dropDownList('File[game_id]', null, $gameList,
                ['style'=>'width:60%;', 'prompt' => 'Выбор игры', 'id' => 'file-game_id','class' => 'form-control']);
    }
     /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}