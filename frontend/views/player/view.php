<?php

use yii\helpers\Html;
//use yii\bootstrap4\Tabs;
use kartik\widgets\ActiveForm;
use kartik\popover\PopoverX;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model app\models\Player */

$this->title = "Профиль игрока";

?>

<div class="player-view">

    <?php
        if (!$data == null) {

            $model = $data['model'];
            $video = $data['video'];

            if (!$model == null) {
                echo $model->name; echo "<br>";
                echo $model->surname; echo "<br>";
            }else{
                echo "Игрок с переданным ID не найден";
            }
        }
    ?>

    <div class="row" style="padding:50px;">

       <?php
            if (!$data == null) {

                $model = $data['model'];
                $video = $data['video'];

                if (!$model == null) {

                    echo TabsX::widget([
                        'items' => [
                            [
                                'label' => 'Видео',
//                                                            'content' => 'проверка',
                                'content' => $this->render('_tab_video',
                                    // передаем данные дальше во вьюху
                                    [
                                        'model' => $model,
                                        'video' => $video
                                    ],
                                    true),
                            ],
                            [
                                'label' => 'Заголовок вкладки 2',
                                'content' => 'Вкладка 2'
                            ],
                        ], //Items
                        'position'=>TabsX::POS_ABOVE,
                        'encodeLabels'=>false
                    ]);//widget
                } //if !$model == null
            } //if !$data == null

       ?>



    </div>
</div>
