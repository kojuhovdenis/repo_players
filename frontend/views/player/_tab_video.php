<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use kartik\popover\PopoverX;
use yii\helpers\Html;
/* 
 * Вкладка просмотра видео голов игрока
 */

?>
<?php

PopoverX::begin([
    'size' => 'lg',
    'header' => 'Видео PopoverX',
    'id' => 'video-dialog',
    //'content' => $content,
    'options'=>['style'=>'position:absolute;top:10%;left:55%;width:750px;height:450px; max-width:1500px;']
]);  
?>
<div class='video-content' style="text-align: center">
    <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vlDzYIIOYmM"
                allowfullscreen></iframe>
    </div>
</div>
<?php PopoverX::end(); ?>

<h3>Видео забитых голов</h3>

<?= GridView::widget([
    'dataProvider' => $video,
    'tableOptions'=>['class'=>'table table-condensed table-hover video-goal', 'style'=>"box-shadow: 0 0 10px rgba(0,0,0,0.5);"],
    'columns' => [
       ['class' => 'yii\grid\SerialColumn'],     
        [
            'attribute' => 'game',
            'label' => 'Матч'
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'value'=>function ($data) {
                return date('d.m.Y',strtotime($data['date']));
            }
        ],
       [
            'attribute' => 'place',
            'label' => 'Место проведения'
        ],
        [
            'attribute' => 'minute',
            'label' => 'Минута матча'
        ],
        [
            'attribute' => 'file-id',
            'label' => 'Видео',
            'value' => function ($data) {
                $link = Html::a('<span class="glyphicon glyphicon-facetime-video"> ' . $data['name'] .  ' </span>', null,
                                 [
                                     'class' => 'noPrint',
                                     'title'=>'Просмотр видео',
                                     'data-toggle'=>'tooltip',
                                     'onclick' => "$('.video-goal').find('tr').css('background-color', '');$(this).parents('tr').css('background-color', '#b2dba1');viewVideo({$data['id']})",
                                     'style'=>'cursor: pointer'
                                 ]);

                return $link;
            },
            'format'=>'raw'
        ],



    ],
]);
?>
<?= $this->render('_modal', ['id' => $model->id]); ?>

<script type="text/javascript">

    //Тут пришлось окно скрыть, иначе оно всегда
    //открывается при загрузке страницы
    $("#video-dialog").hide();

    $('.close').on('click', function(){
            $("#video-dialog").hide();
            $(".video-content").empty();
            $(".video-goal").find('tr').css('background-color', '');
        });
    // пример ! переписать на свой action
    function viewVideo (id){
        console.log("клик по тексту видео");
       $.ajax({
            type: 'POST',
            url: '//'+window.location.host+'/index.php/game/ajax-show-video',
            data: {id:id},
            success: function(data){
                $('.video-content').html(data);
                $("#video-dialog").show();
                document.location.href = "#video-dialog";
            }
        });
    }
</script>
