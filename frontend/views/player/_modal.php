<?php
use yii\helpers\Html;

?>

<!-- Button trigger modal -->
<button type="button" id = "show-Modal" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Выбрать файл для загрузки
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Форма выбора видео</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container py-3">
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="myInput" aria-describedby="myInput">
                            <label class="custom-file-label" for="myInput">Выберите файл</label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>

                <?= Html::tag('button', Html::encode('Загрузить'),
                    [
                        'class' => 'btn btn-primary',
                        'onclick' => "uploadVideo({$id})",
                    ])
                ?>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    /* show file value after file select */
    document.querySelector('.custom-file-input').addEventListener('change', function(e) {
        var fileName = document.getElementById("myInput").files[0].name;
        var nextSibling = e.target.nextElementSibling
        nextSibling.innerText = fileName
    })

    function uploadVideo (id) {
        console.log("Загрузить видео для " + id);

        $.ajax({
            type: 'POST',
            url: '//'+window.location.host+'/index.php/game/ajax-show-video',
            data: {id:id},
            success: function(data){
                $('.video-content').html(data);
                $("#video-dialog").show();
                document.location.href = "#video-dialog";
            }
    }

    // $("#UploadFile").on('click',
    //     function UploadVideo (id){
    //     console.log("Загрузить видео" + id);
    //     //
    //     });
    // }

    $("#show-Modal").on('click', function(){
        $("#exampleModal").removeClass('fade');
    });
</script>