<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Страница ID игроков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
        <?php

        //\frontend\controllers\debug($players);

        foreach ($players as $player) {

           // \frontend\controllers\debug($player);

            echo '<tr>';
            echo '<th scope="row">' . $player['id'] . '</th>';
            echo '<td>' . $player['name'] . '</td>';
            echo '</tr>';
        }
        ?>

        </tbody>
    </table>

    <input class="form-control" id="input_text" type="text" placeholder="Введите имя игрока" aria-label="default input example">
    <br>

    <button class="btn btn-success" id="btn"> Добавить </button>

    <code><?= __FILE__ ?></code>
</div>

<?php

$js = <<<JS
    $('#btn').on('click', function (){
        $.ajax({
            url: 'index.php?r=player/index',
            data: {test: document.getElementById('input_text').value},
            method: 'GET',
            success: function (res){
                console.log(res);
            },
            error: function (){
                alert('Error!');
            }        
        });
    });
JS;

$this->registerJs($js);

?>

