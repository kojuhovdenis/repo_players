<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string $surname
 * @property string $name
 * @property string|null $patronymic
 * @property string|null $date_of_birth
 * @property string|null $nationality
 * @property int|null $height
 * @property int|null $weight
 * @property string|null $school
 * @property string|null $address
 * @property string|null $date_entrance
 * @property int|null $amplua_id
 * @property int|null $city_id
 * @property int|null $country_id
 * @property string|null $mobile_phone
 * @property string|null $email
 * @property int|null $type_id
 * @property string|null $family_members
 * @property string|null $family_medhistory
 * @property int|null $is_archival
 * @property int|null $arena_id
 * @property string|null $date_create
 * @property string|null $date_archive
 * @property string|null $progress
 * @property string|null $fan_id идентификатор болельщика клуба
 */
class Player extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['surname', 'name'], 'required'],
            [['date_of_birth', 'date_entrance', 'date_create', 'date_archive'], 'safe'],
            [['height', 'weight', 'amplua_id', 'city_id', 'country_id', 'type_id', 'is_archival', 'arena_id'], 'integer'],
            [['surname', 'name', 'patronymic', 'nationality', 'address', 'mobile_phone', 'email', 'progress'], 'string', 'max' => 45],
            [['school', 'fan_id'], 'string', 'max' => 75],
            [['family_members'], 'string', 'max' => 145],
            [['family_medhistory'], 'string', 'max' => 245],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Surname',
            'name' => 'Name',
            'patronymic' => 'Patronymic',
            'date_of_birth' => 'Date Of Birth',
            'nationality' => 'Nationality',
            'height' => 'Height',
            'weight' => 'Weight',
            'school' => 'School',
            'address' => 'Address',
            'date_entrance' => 'Date Entrance',
            'amplua_id' => 'Amplua ID',
            'city_id' => 'City ID',
            'country_id' => 'Country ID',
            'mobile_phone' => 'Mobile Phone',
            'email' => 'Email',
            'type_id' => 'Type ID',
            'family_members' => 'Family Members',
            'family_medhistory' => 'Family Medhistory',
            'is_archival' => 'Is Archival',
            'arena_id' => 'Arena ID',
            'date_create' => 'Date Create',
            'date_archive' => 'Date Archive',
            'progress' => 'Progress',
            'fan_id' => 'Fan ID',
        ];
    }
}
