<?php

use \yii\db\Migration;

class m190124_110300_create_file_tables extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__ . '/../../data/academy_file.sql');
        $this->execute($sql);
    }

    public function safeDown()
    {
        $this->dropTable('file');
        $this->dropTable('file_type');
        $this->dropTable('player');
    }
}
