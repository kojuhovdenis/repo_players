


-- Data exporting was unselected.

-- Dumping structure for table academy.file_type
CREATE TABLE IF NOT EXISTS `file_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table academy.file
CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(125) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `comment` varchar(145) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `training_id` int(11) DEFAULT NULL,
  `exercise_id` int(11) DEFAULT NULL,
  `num` tinyint(4) DEFAULT NULL COMMENT 'порядковый номер упражнения',
  `game_id` int(11) DEFAULT NULL,
  `game_event_id` int(11) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `user_update` int(11) DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_filetype_idx` (`type_id`),
  CONSTRAINT `fk_filetype` FOREIGN KEY (`type_id`) REFERENCES `file_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=569447 DEFAULT CHARSET=utf8;

-- Dumping structure for table academy.player
CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `patronymic` varchar(45) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `height` int(3) DEFAULT NULL,
  `weight` int(3) DEFAULT NULL,
  `school` varchar(75) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `date_entrance` date DEFAULT NULL,
  `amplua_id` int(3) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `mobile_phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `family_members` varchar(145) DEFAULT NULL,
  `family_medhistory` varchar(245) DEFAULT NULL,
  `is_archival` int(1) DEFAULT '0',
  `arena_id` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_archive` datetime DEFAULT NULL,
  `progress` varchar(45) DEFAULT NULL,
  `fan_id` varchar(75) DEFAULT NULL COMMENT 'идентификатор болельщика клуба',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='таблица игроков';

-- Data exporting was unselected.
